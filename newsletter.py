""" Generate responsive HTML emails from Markdown files used in a pelican blog.

Refer to https://pbpython.com/ for the details.

"""
from markdown2 import Markdown
from pathlib import Path
from jinja2 import Environment, FileSystemLoader
from premailer import transform
from argparse import ArgumentParser
from bs4 import BeautifulSoup
from datetime import date

today = date.today().strftime("%Y-%m-%d")

def parse_args():
    """Parse the command line input

    Returns:
        args -- ArgumentParser object
    """

    parser = ArgumentParser(
        description='Generate HTML email from markdown file')
    parser.add_argument('-d', '--date',
                        help='set custom date',
                        default=today)
    args = parser.parse_args()
    return args

def sanitize_date(d):
    try:
        year, month, day = map(int, d.split('-'))
        date1 = date(year, month, day)
    except ValueError as e:
        print(e)
        print("Today will be used:", today)
        yesno("Is it ok?")
        return today

    return date1

def yesno(msg="Continue?"):
    while True:
        qr = input(msg+" y/n: ")
        if qr == '' or not qr[0].lower() in ['y','n']:print('Please answer with yes or no!')
        else:break
    if qr[0].lower() == 'y': pass
    if qr[0].lower() == 'n': exit(1)

def print_args(conf):
    print(conf)

if __name__ == '__main__':
    conf = parse_args()
    date = sanitize_date(conf.date)
    print("date:", date)
    print('Completed')
