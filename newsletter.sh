#!/bin/bash

mkdir -p ./public
mkdir -p ./public/img

pushd ./newsletter
for f in ./markdown/*.md; do
    python ./email_gen.py "$f"
done

popd
cp ./newsletter/html/* ./public
cp ./newsletter/template/img/* ./newsletter/markdown/img/* ./public/img

pushd ./public
rm index.html
tree -H '.' -L 1 --noreport --charset utf-8 > index.html
popd

