		Title: Libreadvice
Date: Thu 07 May 2020 03:18:52 PM CEST
Template: newsletter
URL: newsletter_1.html
save_as: newsletter_!.html

## [Articolo di oggi][art0]

#### [Libera il pensiero, usa LibreOffice][art0]
[![](https://inknos.gitlab.io/libreadvice_newsletter/img/libreoffice.png)][art0]

LibreOffice è una suite per ufficio multipiattaforma e free software prodotta da The Document Foundation. La suite comprende vari programmi per l’elaborazione di testi, fogli di calcolo, presentazioni, grafici e disegni, database e formule matematiche.

Perché pagare per utilizzare Microsoft Office quando esiste un prodotto libero e multi piattaforma che fa le stesse cose se non di più?

Questo è probabilmente uno dei prodotti più famosi e meglio mantenuti e aggiornati di tutta la suite di programmi appartenenti alla categoria del software libero. Non si può rinunciare a questo programma quando i pro sono così tanti e i contro così pochi.

  [Leggi tutto...][art0]
  
## Articoli precedenti

#### [Tutti abbiamo qualcosa da nascondere][art1]

In questa conferenza Greenwald spiega brevemente perché bisogna preoccuparsi della propria privacy, anche se apparentemente una persona non ha niente da nascondere.

  [Leggi tutto...][art1] 

#### [Riconoscimento facciale con John Oliver][art2]

Il riconoscimento facciale è utilizzato in molti campi. Dalla funzione di sblocco del nostro cellulare, all'identificazione di sospetti da parte della polizia al tracciamento passo passo di quello che facciamo da parte dei governi.

  [Leggi tutto...][art2] 

#### [Il software libero. Cosa significa][art3]

Il software libero mette l'etica, la libertà e il rispetto al primo posto.
Per questo, su libreadvice.org, consigliamo e promuoviamo solamente software libero o compatibile con esso.

  [Leggi tutto...][art3] 

#### [Sapete cosa sono i Simulatori di Celle Telefoniche? Sono un pericolo per la privacy][art4]

Tra i mille e uno modi in cui possiamo essere tracciati il più semplice è usare celle telefoniche e geolocalizzazione. Più celle ricetrasmittenti ci sono più è semplice per le compagnie telefoniche e organizzazioni pubbliche o private localizzare e identificare gli individui.

  [Leggi tutto...][art4]
  
#### [Firefox: migliora la privacy con le estensioni][art5]

Firefox permette di essere migliorato installando dei componenti aggiuntivi che estendono le caratteristiche e le funzionalità di un browser già di per sé fantastico.

  [Leggi tutto...][art5] 

## Ultime parole

La varietà di programmi utilizzati ogni giorno è vastissima e gli innumerevoli rischi di essere tracciati o violati nella privacy ormai non si contano più. Per questo è importante sapere quali sono le soluzioni migliori e più rispettose della nostra identità.

Perché, ad esempio, utilizzare suite di scrittura come Microsoft Office quando esiste una soluzione equivalente, se non migliore, libera e gratuita? Perché abbandonarsi alla completa vuolazione della privacy durante questo clamoroso periodo storico? Perché scegliere soluzioni apparentemente convenienti che palesemente violano la nostra identità e si arricchiscono vendendo il nostro nome, il nostro numero di cellulare, la nostra privacy?

Il software di cui parliamo è libero, non invade la privacy degli utilizzatori, non traccia ed è trasparente.

> Ti ticordiamo che per restare aggiornati ed imparare più in fretta non solo è importante leggere e informarsi ma anche parlare e condividere le proprie idee. Per questo motivo abbiamo creato canali di discussione e/o di lettura.

> Inoltre, per noi è fondamentale ricevere un riscontro di quello che stiamo facendo. Ci permette di migliorare e di crescere. Non obblighiamo nessuno a farlo, per questo, queste email non sono tracciate in alcun modo, e anche il contenuto stesso della mail è pubblico e visitabile online, proprio come dovrebbe essere ogni informazione: trasparente.

> Sotto queste righe lasciamo i canali dove potrai, se vorrai, partecipare o anche solo sbirciare senza farne parte. In questo momento molte persone si stanno aggiungendo e vanno mano a mano a crearsi piccole discussioni dalle quali tutti, noi compresi, abbiamo qualcosa da imparare.


[art0]: <https://libreadvice.org/libreoffice-guida/>
[art1]: <https://www.ted.com/talks/glenn_greenwald_why_privacy_matters>
[art2]: <https://invidio.us/watch?v=jZjmlJPJgug>
[art3]: <https://libreadvice.org/free-software/>
[art4]: <https://www.eff.org/deeplinks/2020/06/quick-and-dirty-guide-cell-phone-surveillance-protests>
[art5]: <https://libreadvice.org/estensioni-firefox/>
[art6]: <https://libreadvice.org>

[web]:   <https://libreadvice.org>
[tg]: <t.me/libreadvice>
[tg-it]: <t.me/libreadvice_it>
[tg-en]: <t.me/libreadvice_en>

[mastodon-it]: <https://mastodon.uno/@libreadvice>
[mastodon-en]: <https://fosstodon.org/@libreadvice>

[matrix]: <https://matrix.to/#/#libreadvice:mozilla.org>
[irc]: <irc://irc.freenode.net/#libreadvice>

[wt]: <https://wt.social/wt/libreadvice>
[pixelfed]: <https://pixelfed.social/libreadvice>

[rss]: <https://pixelfed.social/libreadvice>

[twitter]: <https://twitter.com/libreadvice>
