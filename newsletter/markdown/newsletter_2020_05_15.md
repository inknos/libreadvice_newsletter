Title: Libreadvice
Date: Thu 07 May 2020 03:18:52 PM CEST
Template: newsletter
URL: newsletter_1.html
save_as: newsletter_!.html

## [Articolo di oggi][art0]

#### [Elimina il tracking di YouTube con Invidious][art0]
[![](https://inknos.gitlab.io/libreadvice_newsletter/img/tube-is-watching.png)][art0]

Come è noto, Google e YouTube, tracciano quello che gli utenti vedono, cliccano e dicono. Ogni tanto, fortunatamente è possibile utilizzare i loro prodotti senza subire tracking ma potendo sfruttare allo stesso tempo tutte, o quasi, le funzionalità.

L'idea tanto semplice ma geniale di Invidious è la seguente. Perché non creare un sito che faccia da portale a YouTube ma senza essere YouTube? Si chiama in gergo tecnico "mirror", non è altro che un servizio "uguale" ma virtuale, come se fosse un'immagine riflessa.  

[Leggi tutto...][art0]
  
## Articoli precedenti

#### [Video | YouTube ascolta quello che dici!][art1]

In questo video viene dimostrato che Google/YouTube ascolta tutto quello che diciamo e che sfrutta quello che impara su di non per proporci pubblicità ad hoc.

[Leggi tutto...][art1] 

#### [NewPipe è un'applicazione in grado di sostituire completamente YouTube su Android!][art2]

NewPipe è un'app per Android estremamente funzionale e rispettosa della privacy. Con NewPipe si possono vedere video e anche riprodurli in sottofondo (quindi si può ascoltare la musica da YouTube mentre si fanno altre cose).

[Leggi tutto...][art2] 

#### [Pillole di tecnologia | Che cos'è un mirror][art3]

Um mirror è una replica di una risorsa accessibile online. Un mirror di un sito web è, perciò, una copia identica o quasi identica di un sito web il cui scopo è, semplicemente, clonare l'originale. 

[Leggi tutto...][art3] 

#### [Zoom acquista Keybase ma l'abito non fa il monaco][art4]

Sono passati pochi giorni dagli scandali su Zoom e la sicurezza. La compagnia americana tenta di ricostruire la sua immagine, aquistando Keybase con lo scopo di integrare metodi crittografici finora assenti o lacunosi.
 
  [Leggi tutto...][art4]
  
#### [Il rispetto della privacy è fondamentale, specialmente quando ci mettiamo la faccia.][art5]

Jitsi risolve il problema della privacy. Come? 

  - Per usarlo non è necessario dare alcuna informazione personale
  - Il sistema è basato su una tecnologia libera
  - Si può utilizzare Jitsi su un server personale in modo da non dover condividere i prorpi dati a terze parti
  
  [Leggi tutto...][art5] 

## Ultime parole

In questa settimana la nostra attenzione si sposta sui video e sulla consapevolezza di come si possa essere tracciati anche da un'app innocua come YouTube. A mano a mano vogliamo approfondire blocchi di argomenti sviluppando guide personali e riferimenti esterni trattanti argomenti esterni e attualità nel mondo tecnologico.

Ti ticordiamo che per restare aggiornati ed imparare più in fretta è solo importante leggere e informarsi ma anche parlare e condividere le proprie idee. Per questo motivo abbiamo creato canali di discussione e/o di lettura.

Inoltre, per noi è fondamentale ricevere un riscontro di quello che stiamo facendo. Ci permette di migliorare e di crescere. Non obblighiamo nessuno a farlo, per questo, queste email non sono tracciate in alcun modo, e anche il contenuto stesso della mail è pubblico e visitabile online, proprio come dovrebbe essre.

Sotto queste righe lasciamo i canali dove potrai, se vorrai, partecipare o anche solo sbirciare senza farne parte. In questo momento molte persone si stanno aggiungendo e vanno mano a mano a crearsi piccole discussioni dalle quali tutti, noi compresi, abbiamo qualcosa da imparare.

[art0]: <https://libreadvice.org/invidious-guida/>
[art1]: <https://invidio.us/watch?v=zBnDWSvaQ1I>
[art2]: <https://libreadvice.org/newpipe-guida/>
[art3]: <https://libreadvice.org/che-cose-un-mirror>
[art4]: <https://www.punto-informatico.it/zoom-acquisizione-keybase/>
[art5]: <https://libreadvice.org/jitsi-guida/>
[art6]: <https://libreadvice.org>

[web]:   <https://libreadvice.org>
[tg]: <t.me/libreadvice>
[tg-it]: <t.me/libreadvice_it>
[tg-en]: <t.me/libreadvice_en>

[mastodon-it]: <https://mastodon.uno/@libreadvice>
[mastodon-en]: <https://fosstodon.org/@libreadvice>

[matrix]: <https://matrix.to/#/#libreadvice:mozilla.org>
[irc]: <irc://irc.freenode.net/#libreadvice>

[wt]: <https://wt.social/wt/libreadvice>
[pixelfed]: <https://pixelfed.social/libreadvice>

[rss]: <https://pixelfed.social/libreadvice>

[twitter]: <https://twitter.com/libreadvice>
