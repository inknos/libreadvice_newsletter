Title: Libreadvice
Date: Thu 07 May 2020 03:18:52 PM CEST
Template: newsletter
URL: newsletter_1.html
save_as: newsletter_!.html

## [Articolo di oggi][art0]

#### [Organizza conferenze virtuali con Jitsi][art0]
[![](https://inknos.gitlab.io/libreadvice_newsletter/img/jitsi-guida.png)][art0]

Il rispetto della privacy è fondamentale, specialmente quando ci mettiamo la faccia. Nessuno vorrebbe avere problemi di sicurezza durante una videochiamata personale o di lavoro

Jitsi risolve il problema della privacy. Come? 

  - Per usarlo non è necessario dare alcuna informazione personale
  - Il sistema è basato su una tecnologia libera
  - Si può utilizzare Jitsi su un server personale in modo da non dover condividere i prorpi dati a terze parti

Se questo non dovesse bastare Jitsi aggiunge a tutte queste buone caratteristiche un'interfaccia semplice, la presenza di molte caratteristiche interessanti come la modalità presentazione, conferenze protette con password e lo streaming di contenuti.

Abbiamo preparato una mini guida per spiegare meglio tutto ciò
  
  [Leggi tutto...][art0]
  
## Articoli precedenti

#### [Un progetto italiano per permettere di restare in contatto][art1]
  
  Dato che Jitsi è un software libero, è possibile utilizzarlo all'interno di un proprio server, quindi all'interno di un proprio spazio sul web. Questo ha permesso ai ragazzi di iorestoacasa.work (https://iorestoacasa.work/) di mettere a disposizione dei server Jitsi per permettere agli italiani di affrontare meglio questa quarantena.  
  
  [Leggi tutto...][art1] 

#### [Si possono fare videochiamate usando un'app veramente privata, veramente libera, veramente anonima?][art2]
  Per le persone più paranoiche o che voglionono portare la privacy al livello successivo. La riservatezza raggiunge il livello esperto pur mantenendo un'interfaccia semplice e pulita.
  
  [Leggi tutto...][art2] 

#### [Zoom: un software nocivo per la privacy][art3]
  Per quanto popolare Zoom presenta dei [gravi problemi di privacy](https://www.wired.it/internet/web/2020/04/06/zoom-problemi-sicurezza/) ed è un software proprietario, quindi nessuno può sapere come funzioni esattamente (questo costituisce un altro grande problema di privacy).
  
  [Leggi tutto...][art3] 

#### [Quante password diverse utilizzate ogni giorno? E quanti nomi utenti diversi avete?][art4]

Uno dei metodi utilizzati in un attacco è l'assunzione che, una volta scoperto l'username, esista anche in altri domini e possa essere usato per attacchi successivi.

  [Leggi tutto...][art4]
  
#### [Proteggersi dal tracking online? Ecco un test per capire quanto si è protetti. Spoiler... probabilmente non molto.][art5]
  
Come fare? Tamite questo strumento offerto dalla EFF (Electronic Frontier Foundation) puoi scoprire due aspetti importanti della tua navigazione online:

1. Quanto vieni tracciato normalmente
2. Quanto è facile identificarti
 
  [Leggi tutto...][art5] 

#### [LibrePlanet - Cosa guardare durante il weekend][art6]

Quest'anno LibrePlanet si è svolto grazie a Jitsi, il software di videochat consigliato anche da noi!

Per chi non lo sapesse, il LibrePlanet è uno degli eventi più importanti organizzati dalla Free Software Foundation, la principale organizzazione che lotta per la difesa e promozione del software libero.

  [Leggi tutto...][art6]

## Per concludere

Questa è la prima delle email settimanali di [Libreadvice](web). Per restare aggiornati ed imparare più in fretta è importante condividere le idee. Per questo motivo abbiamo creato canali di discussione e/o di lettura.

In questo momento molte persone si stanno aggiungendo e vanno mano a mano a crearsi piccole discussioni dalle quali tutti, noi compresi, abbiamo qualcosa da imparare.

[art0]: <https://libreadvice.org/jitsi-guida/>
[art1]: <https://iorestoacasa.work/>
[art2]: <https://jami.net/free-as-in-freedom/>
[art3]: <https://www.wired.it/internet/web/2020/03/31/zoom-privacy-facebook/>
[art4]: <https://invidio.us/watch?v=yzGzB-yYKcc>
[art5]: <https://panopticlick.eff.org/>
[art6]: <https://media.libreplanet.org/>
[web]:   <https://libreadvice.org>
[tg]:    <t.me/libreadvice>
[tg-it]: <t.me/libreadvice_it>
[tg-en]: <t.me/libreadvice_en>

[mastodon-it]: <https://mastodon.uno/@libreadvice>
[mastodon-en]: <https://fosstodon.org/@libreadvice>

[matrix]: <https://matrix.to/#/#libreadvice:mozilla.org>
[irc]: <irc://irc.freenode.net/#libreadvice>

[wt]: <https://wt.social/wt/libreadvice>
[pixelfed]: <https://pixelfed.social/libreadvice>

[rss]: <https://pixelfed.social/libreadvice>

[twitter]: <https://twitter.com/libreadvice>
