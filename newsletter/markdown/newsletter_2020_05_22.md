Title: Libreadvice
Date: Thu 07 May 2020 03:18:52 PM CEST
Template: newsletter
URL: newsletter_1.html
save_as: newsletter_!.html

## [Articolo di oggi][art0]

#### [Signal, comunica in sicurezza][art0]
[![](https://inknos.gitlab.io/libreadvice_newsletter/img/Signal2.png)][art0]

Signal è un applicazione multipiattaforma e libera, creata con l’intento di offrire a tutti un’app di messaggistica estremamente sicura e semplice da usare. Oltre ad inviare messaggi, la versione Android di Signal permette di effettuare chiamate audio e video di ottima qualità.

Leggi la nostra guida e scopri come liberarti da Whatsapp e iniziare ad usare subito Signal.

  [Leggi tutto...][art0]
  
## Articoli precedenti

#### [Il telefono ci spia. Quali dati ci ruba e come difendersi?][art1]

In base alle rivelazioni del 2013 di Edward Snowden sono emersi dati evidenti su come le organizzazioni governative tengano traccia, soprattutto negli Stati Uniti, di ingenti quantità di dati privati.

  [Leggi tutto...][art1] 

#### [Signal è stata selezionata dall'UE per essere usata dai suoi dipendenti][art2]

Signal è un'applicazione facile da usare ed estremamente sicura. Proprio questa caratteristica è stata scelta anche dall'Unione Europea che l'ha preferita a Whatsapp come app di messaggistica dei suoi dipendenti.

  [Leggi tutto...][art2] 

#### [Cosa significa essere end to end: quando la privacy è ai massimi livelli][art3]

Recentemente abbiamo sentito parlare molto di crittografia end-to-end specialmente da quando questo protocollo sembra essere stato implementato da WhatsApp. In realtà questo protocollo è stato reso famoso da Signal, di cui via abbiamo parlato un paio di giorni fa.

  [Leggi tutto...][art3] 

#### [Nel 2016 Signal ha permesso di aggirare la censura di Internet in Egitto e negli Emirati Arabi!][art4]

Abbiamo già parlato di Signal e di censura su questo canale. In Egitto nel 2016 la censura impediva alle persone di usufruire dei servizi di messaggistica criptata. Questo era di fatto un grosso problema per la privacy degli utenti. 

  [Leggi tutto...][art4]
  
#### [Un'ottima occasione per contribuire in modo libero e concreto: aiutiamo a tradurre pixelfed!][art5]

Pixelfed è una piattaforma che potrebbe sostituire Instagram e risolverne i problemi legati al tracking!

Quest'app, PixelDroid, ha bisogno di traduzioni in italiano. È un'ottima occasione per contribuire al software libero senza saper programmare.

Se siete interessati ma non sapete come fare scrivete sul nostro canale e vi aiuteremo nel procedimento.
Oppure se conoscete qualcuno che possa essere interessato, inoltrategli il messaggio!

  [Leggi tutto...][art5] 

## Ultime parole

Non è facile al giorno d'oggi trattare tematiche di privacy perché queste, molto spesso, sono controverse e ci sono tensioni molto grandi in gioco docute soprattutto a pressioni politiche, commerciali e etiche. Il messaggio che vorremmo diffondere è l'importanza di preservare la propria identità digitale, una proprietà importantissima e, purtroppo, di rilevanza sottovalutata al giorno d'oggi.

Ti ticordiamo che per restare aggiornati ed imparare più in fretta non solo è importante leggere e informarsi ma anche parlare e condividere le proprie idee. Per questo motivo abbiamo creato canali di discussione e/o di lettura.

Inoltre, per noi è fondamentale ricevere un riscontro di quello che stiamo facendo. Ci permette di migliorare e di crescere. Non obblighiamo nessuno a farlo, per questo, queste email non sono tracciate in alcun modo, e anche il contenuto stesso della mail è pubblico e visitabile online, proprio come dovrebbe essere ogni informazione: trasparente.

Sotto queste righe lasciamo i canali dove potrai, se vorrai, partecipare o anche solo sbirciare senza farne parte. In questo momento molte persone si stanno aggiungendo e vanno mano a mano a crearsi piccole discussioni dalle quali tutti, noi compresi, abbiamo qualcosa da imparare. 

[art0]: <https://libreadvice.org/signal-guida>
[art1]: <https://invidio.us/watch?v=VFns39RXPrU>
[art2]: <https://www.corriere.it/tecnologia/20_febbraio_25/ue-boccia-whatsapp-dipendenti-useranno-signal-come-funziona-f7290f9c-57c2-11ea-a2d7-f1bec9902bd3.shtml>
[art3]: <https://libreadvice.org/end-to-end>
[art4]: <https://www.wired.com/2016/12/encryption-app-signal-fights-censorship-clever-workaround/>
[art5]: <https://github.com/H-PixelDroid/PixelDroid>
[art6]: <https://libreadvice.org>

[web]:   <https://libreadvice.org>
[tg]: <t.me/libreadvice>
[tg-it]: <t.me/libreadvice_it>
[tg-en]: <t.me/libreadvice_en>

[mastodon-it]: <https://mastodon.uno/@libreadvice>
[mastodon-en]: <https://fosstodon.org/@libreadvice>

[matrix]: <https://matrix.to/#/#libreadvice:mozilla.org>
[irc]: <irc://irc.freenode.net/#libreadvice>

[wt]: <https://wt.social/wt/libreadvice>
[pixelfed]: <https://pixelfed.social/libreadvice>

[rss]: <https://pixelfed.social/libreadvice>

[twitter]: <https://twitter.com/libreadvice>
