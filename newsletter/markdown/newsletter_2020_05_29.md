Title: Libreadvice
Date: Thu 07 May 2020 03:18:52 PM CEST
Template: newsletter
URL: newsletter_1.html
save_as: newsletter_!.html

## [Articolo di oggi][art0]

#### [ProtonMail: un servizio estremamente sicuro e facile da utilizzare][art0]
[![](https://inknos.gitlab.io/libreadvice_newsletter/img/protonmail.jpg)][art0]

Sappiamo che è un passo importante, ma consigliamo di lasciare il prima possibile servizi di posta elettronica come Gmail o Outlook e passare ad altri più sicuri e liberi.

[Leggi tutto...][art0]
  
## Articoli precedenti

#### [Pensi che la tua email sia privata? Pensaci due volte][art1]

Andy Yen è un fisico nucleare; a seguito delle rivelazioni di Edward Snowden ha deciso di fondare ProtonMail, un servizio molto potente e molto facile da usare che permette agli utenti di avere un controllo della privacy superiore alla norma e con pochi click.

[Leggi tutto...][art1] 

#### [Ci sono tanti servizi di posta elettronica sicuri e interessanti!][art2]

Nei giorni scorsi abbiamo consigliato ProtonMail, consigliato anche da privacytools, ma ci sono altri servizi che oltre ad mettere la sicurezza al primo posto offrono altre caratteristiche. È il caso per esempio di Posteo che è alimentato completamente da energie rinnovabili. 

  [Leggi tutto...][art2] 

#### [Cosa si intende quando si dice protocollo: un po' di luce su ciò che usiamo tutti i giorni senza accorgercene][art3]

Un protocollo, in informatica, è un insieme di regole definite al fine di gestire la comunicazione tra dispositivi.

Un protocollo è perciò nient'altro che un regolamento, un manuale di regole e costumi da seguire. In alcuni casi può essere molto restrittivo, come una legge, in altri meno come l'educazione.

  [Leggi tutto...][art3] 

#### [La guida più libera per imparare come proteggersi utilizzando le email][art4]

Email Self-Defense è uno strumento mantenuto e distribuito dalla FSF (Free Software Foundation) con lo scopo di realizzare una infografica aggiornata e semplice per spiegare il funzionamento delle email.

  [Leggi tutto...][art4]
  
#### [Il telefono ci spia. Quali dati ci ruba e come difendersi?][art5]

Il nostro telefono è la fonte principale di dati personali che vengono rubati, condivisi, incrociati e venduti. Come fare per difendersi?

  [Leggi tutto...][art5] 

## Ultime parole

Questa settimana la nostra attenzione si sposta sulla protezione delle email, uno strumento che è parte integrante della nostra quotidianità. Essendo molto vecchio ha con se una buona solidità ma, con essa, anche molti rischi. Speriamo che con queste guide e spunti di lettura si possa costruire un futuro più protetto e che la consapevolezza dei pericoli della rete porti al miglioramento di queste tecnologie così che vengano usate in maniera resposabile e fruttuosa.

Ti ticordiamo che per restare aggiornati ed imparare più in fretta non solo è importante leggere e informarsi ma anche parlare e condividere le proprie idee. Per questo motivo abbiamo creato canali di discussione e/o di lettura.

Inoltre, per noi è fondamentale ricevere un riscontro di quello che stiamo facendo. Ci permette di migliorare e di crescere. Non obblighiamo nessuno a farlo, per questo, queste email non sono tracciate in alcun modo, e anche il contenuto stesso della mail è pubblico e visitabile online, proprio come dovrebbe essere ogni informazione: trasparente.

Sotto queste righe lasciamo i canali dove potrai, se vorrai, partecipare o anche solo sbirciare senza farne parte. In questo momento molte persone si stanno aggiungendo e vanno mano a mano a crearsi piccole discussioni dalle quali tutti, noi compresi, abbiamo qualcosa da imparare. 

Errata corrige:
C'è stato un errore di invio nella precedente newsletter, ora dovrebbe essere tutto in ordine. Grazie per la pazienza

[art0]: <https://libreadvice.org/proton-mail-guida/>
[art1]: <https://peertube.uno/videos/watch/7e9afd40-69de-491b-a71a-559ee502e512>
[art2]: <https://www.privacytools.io/providers/email/>
[art3]: <https://libreadvice.org/protocollo/	>
[art4]: <https://emailselfdefense.fsf.org/it/>
[art5]: <https://invidio.us/watch?v=VFns39RXPrU>
[art6]: <https://libreadvice.org>

[web]:   <https://libreadvice.org>
[tg]: <t.me/libreadvice>
[tg-it]: <t.me/libreadvice_it>
[tg-en]: <t.me/libreadvice_en>

[mastodon-it]: <https://mastodon.uno/@libreadvice>
[mastodon-en]: <https://fosstodon.org/@libreadvice>

[matrix]: <https://matrix.to/#/#libreadvice:mozilla.org>
[irc]: <irc://irc.freenode.net/#libreadvice>

[wt]: <https://wt.social/wt/libreadvice>
[pixelfed]: <https://pixelfed.social/libreadvice>

[rss]: <https://pixelfed.social/libreadvice>

[twitter]: <https://twitter.com/libreadvice>
