Title: Libreadvice
Date: Thu 07 May 2020 03:18:52 PM CEST
Template: newsletter
URL: newsletter_1.html
save_as: newsletter_!.html

## [Articolo di oggi][art0]

#### [Firefox: migliora la privacy con le estensioni][art0]
[![](https://inknos.gitlab.io/libreadvice_newsletter/img/firefox-estensioni.png)][art0]

Firefox permette di essere migliorato installando dei componenti aggiuntivi che estendono le caratteristiche e le funzionalità di un browser già di per sé fantastico.

In questa nostra guida spieghiamo i vantaggi di estensioni famose e che, secondo noi, dovrebbero essere installate o almeno provate da tutti.

  [Leggi tutto...][art0]
  
## Articoli precedenti

#### [NoScript è una estensione di Firefox usata anche da Edward Snowden][art1]

  Quando navighiamo la nostra privacy e sicurezza sono costantemente in pericolo molto spesso a causa degli script presenti nelle pagine web. Questi script possono essere vettori per attacchi informatici o elementi traccianti.
  
  [Leggi tutto...][art1] 

#### [5 strumenti indispensabili per proteggersi online][art2]

La EFF si impegna nella difesa della privacy e ci suggerisce cinque strumenti che possono proteggerci a partire dal nostro browser.

  [Leggi tutto...][art2] 

#### [Firefox aggiorna la sua lista di estensioni consigliate][art3]

A distanza di un anno Firefox o, nello specifico, la Mozilla foundation, decide di aggiornare la lista di estensioni raccomandate.

  [Leggi tutto...][art3] 

#### [Il Do-Not-Track Act di DuckDuckGo][art4]

Dopo la recente introduzione dell'opzione "do-not-track" su Firefox, DuckDuckGo ha raccolto i dati degli utenti che utilizzano questa opzione.

  [Leggi tutto...][art4]
  
#### [Chi protesta deve tutelarsi il più possibile][art5]

Nel clima di proteste che si sta vivendo in varie parti del mondo, come ad Hong Kong e negli Stati Uniti in seguito all'assassinio di George Floyd, è sempre più importante tutelarsi e difendersi. 

  [Leggi tutto...][art5] 

## Ultime parole

Per tutelare la nostra privacy è importante non solo utilizzare un browser che sia rispettoso della nostra privacy ma anche migliorare ed estenderlo con le estensioni.

Alcune di queste sono più invasive e rendono la navigazione un po' più rugginosa. Tuttavia, i vantaggi sono spesso più degli svantaggi. La protezione della privacy secondo noi è prioritaria rispetto alla convenienza e alla comodità in tutti i casi, nessuno escluso.

> Ti ticordiamo che per restare aggiornati ed imparare più in fretta non solo è importante leggere e informarsi ma anche parlare e condividere le proprie idee. Per questo motivo abbiamo creato canali di discussione e/o di lettura.

> Inoltre, per noi è fondamentale ricevere un riscontro di quello che stiamo facendo. Ci permette di migliorare e di crescere. Non obblighiamo nessuno a farlo, per questo, queste email non sono tracciate in alcun modo, e anche il contenuto stesso della mail è pubblico e visitabile online, proprio come dovrebbe essere ogni informazione: trasparente.

> Sotto queste righe lasciamo i canali dove potrai, se vorrai, partecipare o anche solo sbirciare senza farne parte. In questo momento molte persone si stanno aggiungendo e vanno mano a mano a crearsi piccole discussioni dalle quali tutti, noi compresi, abbiamo qualcosa da imparare.


[art0]: <https://libreadvice.org/estensioni-firefox/>
[art1]: <https://www.computerworld.com/article/2475978/snowden-at-sxsw--we-need-better-encryption-to-save-us-from-the-surveillance-state.html>
[art2]: <https://www.eff.org/deeplinks/2016/09/five-eff-tools-help-you-protect-yourself-online>
[art3]: <https://blog.mozilla.org/addons/2020/06/11/recommended-extensions-recent-additions/>
[art4]: <https://spreadprivacy.com/do-not-track-act-2019/>
[art5]: <https://www.eff.org/deeplinks/2020/06/surveillance-self-defense-attending-protests-age-covid-19>
[art6]: <https://libreadvice.org>

[web]:   <https://libreadvice.org>
[tg]: <t.me/libreadvice>
[tg-it]: <t.me/libreadvice_it>
[tg-en]: <t.me/libreadvice_en>

[mastodon-it]: <https://mastodon.uno/@libreadvice>
[mastodon-en]: <https://fosstodon.org/@libreadvice>

[matrix]: <https://matrix.to/#/#libreadvice:mozilla.org>
[irc]: <irc://irc.freenode.net/#libreadvice>

[wt]: <https://wt.social/wt/libreadvice>
[pixelfed]: <https://pixelfed.social/libreadvice>

[rss]: <https://pixelfed.social/libreadvice>

[twitter]: <https://twitter.com/libreadvice>
