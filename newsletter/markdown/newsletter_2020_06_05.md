Title: Libreadvice
Date: Thu 07 May 2020 03:18:52 PM CEST
Template: newsletter
URL: newsletter_1.html
save_as: newsletter_!.html

## [Articolo di oggi][art0]

#### [Libera il tuo telefono grazie a F-Droid!][art0]
[![](https://inknos.gitlab.io/libreadvice_newsletter/img/F-Droid.png)][art0]

F-Droid è uno store come il Google Play Store, ma al suo interno sono presenti solo applicazioni software libero.

All'interno suo interno sono presenti e possono essere scaricate tantissime applicazioni in grado di sostituire completamente le alternative proprietarie e poco rispettose della privacy che si trovano su comunemente sugli altri store.

  [Leggi tutto...][art0]
  
## Articoli precedenti

#### [Stop Watching Us! Un invito alla privacy][art1]

  Questo video è un appello alla privacy e al rispetto dei cittadini da parte dei governi. Cos'è cambiato dal 2013? Non molto, purtoppo, ma una piccola parte si è lentamente mobilitata. In questa settimana  scopriamo come proteggere il nostro smartphone.  
  
  [Leggi tutto...][art1] 

#### [Domande e risposte sul contact tracing di Apple e Google][art2]

La EFF (Electronic Frontier Foundation) ci permette di approfondire l'argomento del contact tracing tanto discusso cercando di spiegare come dovrebbe funzionare.

  [Leggi tutto...][art2] 

#### [Cos'è un software manager e che cosa succede alle nostre spalle][art3]

Un software manager è un programma che permette di cercare, installare e aggiornare altri programmi sul tuo PC o smartphone in modo semplice e veloce. 

  [Leggi tutto...][art3] 

#### [Quando si protesta bisogna tutelarsi il più possibile][art4]

Nel clima di proteste che è presente in varie parti del mondo, come Hong Kong e negli Stati Uniti in seguito all'assassinio di George Floyd, è sempre più importante tutelarsi e difendersi. 

  [Leggi tutto...][art4]
  
#### [ProtonMail è un servizio estremamente sicuro e facile da utilizzare][art5]

ProtonMail si pone come una soluzione ottima, dal momento che è estremamente facile da usare e permette di avere degli standard di sicurezza elevati. 

  [Leggi tutto...][art5] 

## Ultime parole

Il nostro smartphone, che ci piaccia o no, è lo strumento più potente e, allo stesso tempo, più pericoloso. Grazie ad esso possiamo connetterci ai poli opposti della terra ma rischiamo anche di esporre la nostra privacy a molti rischi. Alcuni di questi rischi sono dovuti alle compagnie che vendono gli stessi smartphone e i software in essi.

Per noi è di primaria importanza essere consapevoli dei rischi che si corrono ad utilizzare impropriamente uno smartphone. Per questo motivo, durante la settimana, ci siamo concentrati sull'argomento e abbiamo cercato di 	approfondire il più possibile.

Non sottovalutare mai l'importanza delle informazioni contenute nel tuo telefono. Anche se pensi di avere solo qualche foto innocua o i messaggi con gli amici e colleghi ricordati: ogni volta che puoi non esporre mai ciò che è privato. La privacy è un bene prezioso il cui valore è incalcolabile.

> Ti ticordiamo che per restare aggiornati ed imparare più in fretta non solo è importante leggere e informarsi ma anche parlare e condividere le proprie idee. Per questo motivo abbiamo creato canali di discussione e/o di lettura.
> 
> Inoltre, per noi è fondamentale ricevere un riscontro di quello che stiamo facendo. Ci permette di migliorare e di crescere. Non obblighiamo nessuno a farlo, per questo, queste email non sono tracciate in alcun modo, e anche il contenuto stesso della mail è pubblico e visitabile online, proprio come dovrebbe essere ogni informazione: trasparente.
> 
> Sotto queste righe lasciamo i canali dove potrai, se vorrai, partecipare o anche solo sbirciare senza farne parte. In questo momento molte persone si stanno aggiungendo e vanno mano a mano a crearsi piccole discussioni dalle quali tutti, noi compresi, abbiamo qualcosa da imparare.

[art0]: <https://libreadvice.org/f-droid-guida/>
[art1]: <https://invidio.us/watch?v=aGmiw_rrNxk>
[art2]: <https://www.eff.org/deeplinks/2020/04/apple-and-googles-covid-19-exposure-notification-api-questions-and-answers>
[art3]: <https://libreadvice.org/software-manager/>
[art4]: <https://www.eff.org/deeplinks/2020/06/surveillance-self-defense-attending-protests-age-covid-19>
[art5]: <https://libreadvice.org/proton-mail-guida/>
[art6]: <https://libreadvice.org>

[web]:   <https://libreadvice.org>
[tg]: <t.me/libreadvice>
[tg-it]: <t.me/libreadvice_it>
[tg-en]: <t.me/libreadvice_en>

[mastodon-it]: <https://mastodon.uno/@libreadvice>
[mastodon-en]: <https://fosstodon.org/@libreadvice>

[matrix]: <https://matrix.to/#/#libreadvice:mozilla.org>
[irc]: <irc://irc.freenode.net/#libreadvice>

[wt]: <https://wt.social/wt/libreadvice>
[pixelfed]: <https://pixelfed.social/libreadvice>

[rss]: <https://pixelfed.social/libreadvice>

[twitter]: <https://twitter.com/libreadvice>
